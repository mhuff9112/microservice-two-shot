import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

// async function loadAPIData(){
//   const shoeResponse=await fetch("http://localhost:8080/api/shoes/");

//   if (shoeResponse.ok) {
//     const shoeData = await shoeResponse.json();
//     root.render(
//       <React.StrictMode>
//         <App shoeList={shoeData.shoes} />
//       </React.StrictMode>
//     );
//   } else {
//     console.error(shoeResponse)
//   }
//   }
// loadAPIData();