import React, { useEffect, useState } from "react";
import { useParams, useNavigate } from "react-router-dom";

function HatDetail(props){
    const { hatId } = useParams();
    const navigate = useNavigate();

    const [hat, setHat] = useState();

    useEffect(() => {
        const getHat = async () => {
            const response = await fetch(`http://localhost:8090/api/hats/${hatId}/`)
            const data = await response.json();
            setHat(data);
        };
        getHat();
    }, [hatId]);
    
    if (!hat){
        return <p>Hat not Found!</p>
    }
    return(
        <div className="card shadow">
            <div className='card mx-auto' style={{width: "40rem"}}>
                <img src={hat.picture_url} className='card-img-top' alt="HatIm" style={{objectFit: "cover" }} />
                <div className="card-body text-center">
                    <h5 className="card-title">{hat.style_name}: {hat.color}</h5>
                    <p className="card-text">Fabric: {hat.fabric}</p>
                    <p className="card-text">Closet: {hat.location.closet_name}</p>
                    <p className="card-text">Section: {hat.location.section_number}</p>
                    <p className="card-text">Section: {hat.location.shelf_number}</p>
                    <button onClick={() => navigate(-1)} className='btn btn-primary'>Go Back</button>
                </div>
            </div>
        </div>
    );
}

export default HatDetail;