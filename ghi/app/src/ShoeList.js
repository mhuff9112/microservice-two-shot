import React, { useEffect, useState } from 'react';

function ShoeList({ shoes, setShoes }) {
  const handleDelete = async (shoeId) => {
    const response = await fetch(`http://localhost:8080/api/shoes/${shoeId}/`, {
      method: 'DELETE',
    });
    if (response.ok) {
      const updateShoes = shoes.filter((shoe) => shoe.id !== parseInt(shoeId));
      setShoes(updateShoes);
    }
  };

  useEffect(() => {
    const fetchData = async () => {
      const response = await fetch('http://localhost:8080/api/shoes');

      if (response.ok) {
        const data = await response.json();
        setShoes(data.shoes);
      } else {
        console.error(response);
      }
    };

    fetchData();
  }, []);

  return (
    <div>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Manufacturer</th>
            <th>Model Name</th>
            <th>Color</th>
          </tr>
        </thead>
        <tbody>
          {shoes.map((shoe) => {
            return (
              <tr key={shoe.id}>
                <td>{shoe.manufacturer}</td>
                <td>{shoe.model_name}</td>
                <td>{shoe.color}</td>
                <td>
                  <img src={shoe.picture_url} width="100" alt="" />
                </td>
                <td>
                  <button onClick={() => handleDelete(shoe.id)}>Delete</button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}

export default ShoeList;
