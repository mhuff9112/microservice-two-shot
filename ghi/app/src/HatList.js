import React, { useEffect, useState } from 'react';
import { Link, NavLink } from 'react-router-dom';

function HatsList(props) {
  const [hats, setHats] = useState(props.hats);

  const handleDelete = async (hatId) => {
    const response = await fetch(`http://localhost:8090/api/hats/${hatId}/`, {
      method: 'DELETE',
    });
    if (response.ok) {
      const updatedHats = hats.filter(hat => hat.id !== parseInt(hatId));
      setHats(updatedHats);
    }
  };

  const getData = async () => {
    const hatsUrl = "http://localhost:8090/api/hats/";
    const response = await fetch(hatsUrl);

    if (response.ok) {
      const data = await response.json();
      setHats(data.hats)
    }
  }

  useEffect(() => {
    getData()
  }, []);

  return (
    <div className="container my-4">
      <h1 className="text-center mb-4">List of Hats</h1>
      <div className="text-center">
        <NavLink to="/hats/new" className="btn btn-primary">
          Create New Hat
        </NavLink>
      </div>
      <div className="row">
        {hats.map((hat) => { 
            return (
                <div className="col-md-4 mb-4" key={hat.id}>
                    <div className="card shadow">
                        <Link to={`${hat.id}`} style={{ textDecoration: 'none', color: 'black' }}>
                            <img
                            src={hat.picture_url}
                            className="card-img-top"
                            alt={`${hat.manufacturer} ${hat.model_name}`}
                            style={{ objectFit: "cover" }}
                            />
                        </Link>
                        <div className="card-body text-center">
                            <Link to={`${hat.id}`} style={{ textDecoration: 'none', color: 'black' }}>
                            <h5 className="card-title">{hat.manufacturer} {hat.model_name}</h5>
                            <p className="card-text">Style: {hat.style_name}</p>
                            <p className="card-text">Fabric: {hat.fabric}</p>
                            <p className="card-text">Color: {hat.color}</p>
                            </Link>
                            <button
                            type="button"
                            className="btn btn-danger"
                            onClick={() => handleDelete(hat.id)}
                            >
                            Delete
                            </button>
                        </div>
                    </div>
                </div>
            )})}
      </div>
    </div>
  );
}

export default HatsList;