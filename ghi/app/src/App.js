import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import HatsList from './HatList';
import HatDetail from './HatDetail';
import NewHatForm from './NewHatForm';
import ShoeList from './ShoeList';
import NewShoeForm from './NewShoeForm';
import { useEffect, useState } from 'react'

function App() {
  const [hats, setHats] = useState([]);

  const getHats = async () => {
    const response = await fetch('http://localhost:8090/api/hats')

    if (response.ok){
      const data = await response.json();
      setHats(data.hats);
    }
    else{console.error(response);
    }
  }

  const [shoes, setShoes] = useState([]);
  const getShoes = async () => {
    const response = await fetch('http://localhost:8080/api/shoes')

    if (response.ok) {
      const data = await response.json();
      setShoes(data.shoes);
    }
    else {
      console.error(response);
    }
  }

  const handleCreateHat = (newHat) => {
    setHats([...hats, newHat]);
  }

  useEffect(() => {
    getHats();
    getShoes ();
  }, []);


  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
            <Route path="hats">
              <Route index element={<HatsList hats={hats} setHats={setHats} />} />
              <Route path=':hatId' element= {<HatDetail hats={hats} setHats={setHats} />} />
              <Route path='new' element= {<NewHatForm onCreate={handleCreateHat} />} />
            </Route>
            <Route path="/shoes">
              <Route index element={<ShoeList shoes={shoes} setShoes={setShoes}/>} />
              <Route path="new" element= {<NewShoeForm />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
