import React, {useEffect, useState } from 'react';

function ShoeForm(props){

    const [bins, setBins] = useState([]);

    const [manufacturer, setManufacturer] = useState('');
    const [modelName, setModelName] = useState('');
    const [color, setColor] = useState('');
    const [pictureUrl, setPictureUrl] = useState('');
    const [bin, setBin] = useState('');

    const fetchData = async () => {
        const response = await fetch('http://localhost:8100/api/bins')

        if (response.ok) {
            const data = await response.json();
            setBins(data.bins)
        } else {
            console.log("Wrong!", response)
        }
    }

    const handleManufacturer = (event) => {
        const value = event.target.value;
        setManufacturer(value);
    }

    const handleModelName = (event) => {
        const value = event.target.value;
        setModelName(value);
    }

    const handleColor = (event) => {
        const value = event.target.value;
        setColor(value);
    }

    const handlePictureUrl = (event) => {
        const value = event.target.value;
        setPictureUrl(value);
    }

    const handleBin = (event) => {
        const value = event.target.value;
        setBin(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.manufacturer = manufacturer;
        data.model_name = modelName;
        data.color = color;
        data.picture_url = pictureUrl;
        data.bin = bin;
        console.log(data);

        const shoeUrl = "http://localhost:8080/api/shoes/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(shoeUrl, fetchConfig);
        if (response.ok) {
            const newShoe = await response.json();
            console.log(newShoe);

            setManufacturer('');
            setModelName('');
            setColor('');
            setPictureUrl('');
            setBin('');
        } else {
            console.log("Wrong!", response);
        }

    };

useEffect(() => {
    fetchData();

}, []);

return (
    <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Add a Shoe</h1>
                <form onSubmit={handleSubmit} id="add-shoe-form">
                    <div className="form-floating mb-3">
                        <input
                        onChange={handleManufacturer}
                        value={manufacturer}
                        required
                        type="text"
                        placeholder="Manufacturer"
                        id="manfacturer"
                        className="form-control"/>
                        <label htmlFor="manfacturer">Manufacturer</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input
                        onChange={handleModelName}
                        value={modelName}
                        required
                        type="text"
                        placeholder="Model Name"
                        id="modelName"
                        className="form-control" />
                        <label htmlFor="modelName">Model Name</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input
                        onChange={handleColor}
                        value={color}
                        required
                        type="text"
                        placeholder="Color"
                        id="color"
                        className="form-control" />
                        <label htmlFor="color">Color</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input
                        onChange={handlePictureUrl}
                        value={pictureUrl}
                        required
                        type="text"
                        placeholder="Picture URL"
                        id="pictureUrl"
                        className="form-control"/>
                        <label htmlFor="pictureUrl">Picture URL</label>
                    </div>
                    <div className="form-floating mb-3">
                        <select onChange={handleBin}
                        value={bin}
                        name="bin"
                        id="bin"
                        required>
                            <option value="">Choose a bin</option>
                                {bins.map((bin) => {
                                    return (
                                        <option key={bin.href} value={bin.href}>
                                            {bin.closet_name}
                                        </option>
                                    );
                                })}
                        </select>
                    </div>
                    <button className="btn btn-primary">Add</button>
                </form>
            </div>
        </div>
    </div>
    );
                        }; 
export default ShoeForm;