from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from common.json import ModelEncoder
from .models import LocationVO, Hats

class LocationVOEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "closet_name",
        "section_number",
        "shelf_number",
        "import_href",
    ]

class HatsListEncoder(ModelEncoder):
    model = Hats
    properties = [
        "fabric",
        "style_name",
        "color",
        "picture_url",
        "id"
    ]
    
    def get_extra_data(self, o):
        return {"location": o.location.closet_name}

class HatsDetailEncoder(ModelEncoder):
    model = Hats
    properties = [
        "style_name",
        "color",
        "picture_url",
        "fabric",
        "location",
        
    ]    
    encoders = {
        "location": LocationVOEncoder(),
    }
    

#GET a List of Hats
@require_http_methods(["GET", "POST"])
def api_list_hats (request, location_vo_id=None):
    if request.method == "GET":
        if location_vo_id is not None:
            hats = Hats.objects.filter(location=location_vo_id)
        else:
            hats = Hats.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatsListEncoder
        )
    else: #POST
        content = json.loads(request.body)
        try:
            location_href = content["location"]
            print(location_href)
            location = LocationVO.objects.get(import_href=location_href)
            content["location"] = location
        except:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )
        hats = Hats.objects.create(**content)
        return JsonResponse(
            hats,
            encoder=HatsDetailEncoder,
            safe=False,

        )

@require_http_methods(["DELETE", "GET"])
def api_show_hats(request, pk):
    if request.method == "GET":
        hats = Hats.objects.get(id=pk)
        return JsonResponse(
            hats,
            encoder= HatsDetailEncoder,
            safe=False,
        )
    else: # DELETE
        count, _ = Hats.objects.get(id=pk).delete()
        return JsonResponse({"delete": count > 0})