from django.db import models
from django.urls import reverse


class LocationVO(models.Model):
    closet_name = models.CharField(max_length=100)
    section_number = models.PositiveSmallIntegerField()
    shelf_number = models.PositiveSmallIntegerField()
    import_href = models.CharField(max_length=200, unique=True)
    
    def __str__(self):
        return self.closet_name
    


class Hats(models.Model):
    fabric = models.CharField(max_length=200)
    style_name = models.CharField(max_length=200)
    color = models.CharField(max_length=200)
    picture_url= models.URLField(null=True, blank=True)
    location = models.ForeignKey(
        LocationVO, 
        on_delete=models.CASCADE,
        null=True,
        blank=True)

    
    def __str__(self):
        return self.style_name
    
    def get_api_url(self):
        return reverse("api_show_hat", kwargs={"pk": self.pk})
    