# Wardrobify

Team:

* Matthew Huff - Hats
* Shane McCracken - Shoes

## Design

## Shoes microservice

Explain your models and integration with the wardrobe
microservice, here.
In this project, I created two models, BinVO and Shoe, each containing the objects needed to create unique shoes in our Wardobify. Then, I made two view functions with GET, which would get the list of shoes that the user-added, and POST, which is used to create a new shoe to our shoe list. Additionally, I also created three model encoders, BinVODetailEncoder, ShoeDetailEncoder, and ShoeListEncoder to convert my data. After building the models and their corresponding view functions, I used React to build a ShoeList Javascript (.js) file that fetched information from the models and their correlating App Javascript(.js) file, which was displayed in a table. Furthermore, I also created a delete button and its functionality for shoes that the user wishes to remove from the list. Upon completing the previously mentioned parts, I also made ShoeForm Javascript (.js) file using a fetch to categorize shoes in their respective bins.

## Hats microservice

Explain your models and integration with the wardrobe
microservice, here.
